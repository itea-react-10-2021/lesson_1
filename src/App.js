import React from 'react';
import './App.css';

import Post from './post/Post'
import guests from './guests.json';

let data = [ 
	{ title: "Hello", display: false },
	{ title: "World", display: true },
	{ title: "JavaScript", display: true },
];


class App extends React.Component {

	state = {
		search : "",
		guests
	}

	changeHandler = ( event ) => {
		this.setState({
			search: event.target.value
		})
	}

	render = () => {
		const { search } = this.state;

		return(
			<>
				<div>
					<input 
						onChange={this.changeHandler}
						placeholder="Search..."
						value={ search }
					/>
				</div>

				{
					data.map( ({ title, display }, index ) => 
						<Post 
							key={ index }
							title={ title }
							display={ display }
						/> 
					)
				}        
			</>
		);
	}

}


export default App;
