import React, { Component } from 'react'
import './post.css';

class Post extends Component {

    state = {
        likes: 0
    }

    addLike = () => {
        this.setState({
            likes: this.state.likes + 1
        });
    }

    render = () => {
        const { addLike } = this;
        const { likes } = this.state;
        const { title, display } = this.props;

        return(
            <div className="post" style={ display ? { backgroundColor: "green" } : {  backgroundColor: "red" }}>
                <h1> { title } World </h1>
                {
                    display && (
                        <button onClick={addLike} > Add Like { likes }</button> 
                    )
                }
            </div>
        );
    }

}


export default Post;